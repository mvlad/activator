SUMMARY     = "AGL Native app testing Application"
DESCRIPTION = "AGL Native app testing with recipe method"
HOMEPAGE    = "http://docs.automotivelinux.org"
LICENSE     = "Apache-2.0"
SECTION     = "apps"
LIC_FILES_CHKSUM = "file://COPYING;md5=33537927e210002ca65a9d0e77fc26ae"

DEPENDS = "\
        qtbase \
        qtdeclarative \
        qtquickcontrols2 \
        libqtappfw \
	libafb-helpers-qt \
	wayland-native wayland qtwayland qtwayland-native \
"

inherit qmake5 systemd pkgconfig aglwgt

SRC_URI = "git://git@gitlab.collabora.com/mvlad/activator.git;protocol=ssh;branch=master"
SRCREV  = "9340733ea03f4235304e494e9b8a80fba6d741a9"

PV      = "1.0+git${SRCPV}"
S       = "${WORKDIR}/git/"

PATH_prepend = "${STAGING_DIR_NATIVE}${OE_QMAKE_PATH_QT_BINS}:"

OE_QMAKE_CXXFLAGS_append = " ${@bb.utils.contains('DISTRO_FEATURES', 'agl-devel', '' , '-DQT_NO_DEBUG_OUTPUT', d)}"
