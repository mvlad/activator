import QtQuick 2.2
import QtQuick.Window 2.11
import QtQuick.Layouts 1.1

Window {
	visible: true
	flags: Qt.FramelessWindowHint

	width: Screen.width
	height: Screen.height

	title: 'main main'
	color: 'yellow'

	// obviously this has to run
	property string app_id: AppId

	// wait 2.5 seconds before trying to switch to another application
	Timer {
		id: timer
		interval: 2500

		running: true
		repeat: false

		onTriggered: {
			console.log("Timer expired, switching to " + app_id + ", window is " + Window.window)
			// note that Window.window is null here so in activate_app
			// we take the first output in activate_app()
			shell.activate_app(Window.window, app_id)
		}
	}

}
