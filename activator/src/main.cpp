#include <QGuiApplication>
#include <QCommandLineParser>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlComponent>
#include <QtQml/qqml.h>
#include <QWindow>
#include <QQuickWindow>
#include <QScreen>
#include <QTimer>
#include <qpa/qplatformnativeinterface.h>

#include <cstdlib>
#include <cstring>
#include <memory>
#include <wayland-client.h>

#include "wayland-agl-shell-desktop-client-protocol.h"
#include "shell-desktop.h"

// this and the agl-shell extension should be added in some kind of a wrapper
// for easy usage
static void global_add(void *data, struct wl_registry *reg, uint32_t name,
		       const char *interface, uint32_t version)
{
	struct agl_shell_desktop **shell =
		static_cast<struct agl_shell_desktop **>(data);

	if (strcmp(interface, agl_shell_desktop_interface.name) == 0) {
		*shell = static_cast<struct agl_shell_desktop *>(
			wl_registry_bind(reg, name, &agl_shell_desktop_interface, version)
		);
	}
}

static void global_remove(void *data, struct wl_registry *reg, uint32_t id)
{
	(void) data;
	(void) reg;
	(void) id;
}

static const struct wl_registry_listener registry_listener = {
	global_add,
	global_remove,
};

static void
application_id_event(void *data, struct agl_shell_desktop *agl_shell_desktop,
		     const char *app_id)
{
	(void) data;
	(void) agl_shell_desktop;

	// should probably add here to a list the application or trigger emit
	// for QML code, also note that we get here our own application

	if (strcmp(app_id, "activator") == 0)
		return;

	qInfo() << "app_id: " << app_id;
}

static void
application_state_app(void *data, struct agl_shell_desktop *agl_shell_desktop,
		     const char *app_id, const char *appdata, uint32_t st, uint32_t role)
{

}

static const struct agl_shell_desktop_listener agl_shell_desk_listener = {
	application_id_event,
	application_state_app,
};

static struct agl_shell_desktop *
register_agl_shell_desktop(void)
{
	struct wl_display *wl;
	struct wl_registry *registry;
	struct agl_shell_desktop *shell = nullptr;

	QPlatformNativeInterface *native = qApp->platformNativeInterface();

	wl = static_cast<struct wl_display *>(native->nativeResourceForIntegration("display"));
	registry = wl_display_get_registry(wl);

	wl_registry_add_listener(registry, &registry_listener, &shell);
	// Roundtrip to get all globals advertised by the compositor
	wl_display_roundtrip(wl);
	wl_registry_destroy(registry);

	if (shell) {
		qInfo() << "registered a agl desktop shell listener";
		agl_shell_desktop_add_listener(shell, &agl_shell_desk_listener, NULL);
	}

	return shell;
}

int main(int argc, char *argv[])
{
	setenv("QT_QPA_PLATFORM", "wayland", 1);
	QGuiApplication a(argc, argv);
	QQmlApplicationEngine engine;
	QCommandLineParser parser;

	struct agl_shell_desktop *ashell = register_agl_shell_desktop();

	QString AppId;
	parser.addPositionalArgument("appid", a.translate("main", "application id"));
	parser.addHelpOption();
	parser.addVersionOption();
	parser.process(a);
	QStringList positionalArguments = parser.positionalArguments();
	AppId = positionalArguments.takeFirst();


	if (ashell) {
		qInfo() << "Found agl_shell_desktop extension!";
		std::shared_ptr<struct agl_shell_desktop> shell{ashell, agl_shell_desktop_destroy};

		QQmlContext *context = engine.rootContext();

		// pass the shell to QML to call it from there after some time
		context->setContextProperty("shell", new Shell(shell, &a));
		context->setContextProperty("AppId", AppId);
	}

	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return a.exec();
}
